from app.utils import Database

from flask import current_app, g
from flask.cli import with_appcontext
from click import command, echo

from sqlite3 import connect, Row, PARSE_DECLTYPES
from pathlib import Path

def get_db():
    if g.get("db") is None:
        database_path = current_app.config["DATABASE"]
        g.db = connect(database_path, detect_types=PARSE_DECLTYPES)
        g.db.row_factory = Row

    return Database(database_path, connection=g.db)

def close_db(e=None):
    db = g.pop("db", None)
    if db is not None:
        db.close()

def init_app(app):
    app.teardown_appcontext(close_db)

    database_path = Path(current_app.config["DATABASE"])
    db = Database(database_path)

    app_path = Path(current_app.root_path)
    schema = app_path / "schemas" / "init.sql"
    if not schema.exists():
        raise ValueError(f"{schema} does not exist")

    # NOTE - Load init schema on start
    db.run(schema.read_text())
