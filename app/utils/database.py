#from mysql.connector import connect, Error
from pathlib import Path
from contextlib import closing, contextmanager
from threading import Lock
from sqlite3 import (
    connect,
    PARSE_DECLTYPES,
    PARSE_COLNAMES,
    IntegrityError,
    OperationalError,
    ProgrammingError
)

UNDEFINED = "UNDEFINED"

class LogicError(Exception):
    pass

class Database:
    def __init__(self, path, debug=False, enforce=True, connection=None):
        memory = ":memory:"

        self.path = str(path) if path is not None else memory
        self._debug = debug
        self._enforce = enforce

        self._conn = None
        self._open = False
        if connection is not None:
            self._conn = connection
            self._open = True

        self.is_memory = self.path == memory
        self.keep_open = self.is_memory
        #if self.is_memory or not Path(self.path).exists():
        #    self.create()

    @contextmanager
    def _connection(self):
        settings = dict()
        settings["check_same_thread"] = False
        settings["isolation_level"] = "DEFERRED"
        settings["detect_types"] = PARSE_DECLTYPES | PARSE_COLNAMES

        if not self._open:
            self._conn = connect(self.path, **settings)
            self._open = True

        try:
            # Give control back to caller
            yield

        except Exception as e:
            raise

        else:
            if not self.keep_open:
                self.close()

    @contextmanager
    def _transaction(self):
        # NOTE - Potentially disable this for performance gains
        if self._enforce:
            # Enforce foreign keys
            self._conn.execute("PRAGMA foreign_keys = ON;")

            # Enable WAL mode for journaling
            #self._conn.execute("PRAGMA journal_mode = WAL;")

            # Disable WAL mode for journaling
            #self._conn.execute("PRAGMA journal_mode = delete;")

        # Initiate auto-commit mode
        self._conn.execute("BEGIN")
        try:
            # Give control back to caller
            yield

        except Exception as e:
            # Rollback on failure
            self._conn.rollback()
            raise

        else:
            # Commit on success
            self._conn.commit()

    def close(self):
        self._conn.close()
        self._conn = None
        self._open = False

    def try_exec(self, cursor, args, many=False):
        try:
            if many:
                cursor.executemany(*args)

            else:
                cursor.execute(*args)

            return False, None

        except IntegrityError as e:
            err = format_exc() if self._debug else str(e)
            return True, err

        except OperationalError as e:
            err = format_exc() if self._debug else str(e)
            return True, err

        except ProgrammingError as e:
            err = format_exc() if self._debug else str(e)
            return True, err

        except Exception as e:
            err = format_exc() if self._debug else str(e)
            return True, err

    def run(self, sql_code):
        with self._connection():
            if self._conn is None:
                raise LogicError("No connection is open")

            self._conn.executescript(sql_code)

    def execute(self, command, *args, fetch=False, commit=False,
        default=UNDEFINED):
        with self._connection():
            if self._conn is None:
                raise LogicError("No connection is open")

            with closing(self._conn.cursor()) as cursor:
                #cursor.execute(f"USE {self._database};")
                err = False
                msg = None

                arguments = (command, args) if len(args) > 0 else (command,)
                if commit:
                    with self._transaction():
                        err, msg = self.try_exec(cursor, arguments)

                else:
                    err, msg = self.try_exec(cursor, arguments)

                if not fetch:
                    if err:
                        return err, msg

                    first = command.split()[0].lower()
                    if first == "insert":
                        return False, cursor.lastrowid

                    elif first == "delete":
                        return False, cursor.rowcount

                    return False, cursor

                if err:
                    if default == UNDEFINED:
                        return err, msg

                    return False, default

                result = cursor.fetchall()
                if not result or len(result) == 0:
                    return False, default

                return False, result

        return True, default

    def execute_many(self, command, params):
        with self._connection():
            if self._conn is None:
                raise LogicError("No connection is open")

            with closing(self._conn.cursor()) as cursor:
                #cursor.execute(f"USE {self._database};")
                with self._transaction():
                    arguments = command, params
                    err, msg = self.try_exec(cursor, arguments, many=True)
                    if err:
                        return err, msg

                    return False, cursor

    def read(self, command, *args, default=UNDEFINED):
        kwargs = dict()
        kwargs["fetch"] = True
        kwargs["default"] = default
        kwargs["commit"] = False
        return self.execute(command, *args, **kwargs)

    def read_one(self, command, *args, default=UNDEFINED):
        err, res = self.read(command, *args, default=default)
        if err:
            return err, res

        if res == UNDEFINED:
            return False, UNDEFINED

        return False, res[0] if len(res) > 0 else None

    def write(self, command, *args):
        kwargs = dict()
        kwargs["fetch"] = False
        kwargs["commit"] = True
        return self.execute(command, *args, **kwargs)

    def write_many(self, command, params):
        return self.execute_many(command, params)
