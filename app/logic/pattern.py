def generate_pattern(solution, guess):
    correctList = list(solution)
    guessList = list(guess)
    result = list("00000")
    letters = set(solution)
    dict = {}

    for l in letters:
        dict[l] = solution.count(l)

    for i in range(len(correctList)):
        if guessList[i] == correctList[i]:
            result[i] = 2
            dict[guessList[i]] = dict[guessList[i]] - 1
        elif guessList[i] in correctList and dict[guessList[i]] != 0:
            result[i] = 1
            dict[guessList[i]] = dict[guessList[i]] - 1
        else:
            result[i] = 0

    return result

def check_validity(history, solution, guess):
    invalid = list()
    count = 0
    for key in history:
        for c in list(key):
            if c not in invalid and history[key][count] == 0:
                invalid.append(c)
            count += 1
        count = 0

    for c in list(guess):
        if c in invalid:
            return False, None

    return True, generate_pattern(solution, guess)

def parse_text(text):
    day = text[7:10]
    score = text.split(" ")[2][0:1]
    patterns = []
    guesses = text.split("\n\n")[1].split("\n")

    for guess in guesses:
        pattern = convert_guess(guess)
        patterns.append(pattern)

    return day, score, patterns

def convert_guess(guess):
    guessList = list(guess)
    pattern = []
    for c in guessList:
        if c.encode('utf-8') == b'\xf0\x9f\x9f\xa9':
            pattern.append(2)
        elif c.encode('utf-8') == b'\xf0\x9f\x9f\xa8':
            pattern.append(1)
        else:
            pattern.append(0)

    return pattern