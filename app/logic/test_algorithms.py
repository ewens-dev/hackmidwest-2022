from app.logic import generate_pattern, check_validity, parse_text

def test_pattern_generation():
    pattern = generate_pattern("words", "crate")
    assert pattern == [0,1,0,0,0], f"invalid pattern generated {pattern}"

    pattern = generate_pattern("words", "words")
    assert pattern == [2,2,2,2,2], f"invalid pattern generated {pattern}"

    pattern = generate_pattern("words", "sword")
    assert pattern == [1,1,1,1,1], f"invalid pattern generated {pattern}"

    pattern = generate_pattern("words", "woods")
    assert pattern == [2,2,0,2,2], f"invalid pattern generated {pattern}"

    pattern = generate_pattern("woods", "words")
    assert pattern == [2,2,0,2,2], f"invalid pattern generated {pattern}"

    pattern = generate_pattern("moose", "sword")
    assert pattern == [1,0,2,0,0], f"invalid pattern generated {pattern}"

    pattern = generate_pattern("moose", "mouse")
    assert pattern == [2,2,0,2,2], f"invalid pattern generated {pattern}"

    pattern = generate_pattern("seven", "geese")
    assert pattern == [0,2,1,1,0], f"invalid pattern generated {pattern}"

def test_check_validity():
    solution = "midge"
    history = {
        "crate": [0,0,0,0,2],
        "singe": [0,2,0,2,2],
    }

    guess = "crate"
    valid, pattern = check_validity(history, solution, guess)
    assert valid is False, f"incorrectly marked as valid: {guess}"
    assert pattern is None, f"pattern should not be returned: {pattern}"

    guess = "bilge"
    valid, pattern = check_validity(history, solution, guess)
    assert valid is True, f"incorrectly marked as invalid: {guess}"
    assert pattern == [0,2,0,2,2], f"invalid pattern generated {pattern}"
    history[guess] = pattern

    guess = "midge"
    valid, pattern = check_validity(history, solution, guess)
    assert valid is True, f"incorrectly marked as invalid: {guess}"
    assert pattern == [2,2,2,2,2], f"invalid pattern generated {pattern}"

def test_parse_text():
    text = """
Wordle 399 3/6

⬜⬜⬜⬜🟩
🟨⬜⬜⬜🟩
🟩🟩🟩🟩🟩
    """.strip()

    day, score, patterns = parse_text(text)
    assert day == "399", f"incorrect day parsed: {day}"
    assert score == "3", f"incorrect score parsed: {score}"
    assert patterns == [[0,0,0,0,2],[1,0,0,0,2],[2,2,2,2,2]], f"incorrect patterns parsed: {patterns}"