from .database import init_app
from .routes.ui import bp as ui_bp
from .routes.api import bp as api_bp

from flask import Flask

from pathlib import Path
from os import getenv

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app_path = Path(app.instance_path)

    # pythonic mkdir -p
    app_path.mkdir(parents=True, exist_ok=True)

    secret_key = getenv("SECRET_KEY")
    if secret_key is None:
        raise ValueError("Missing SECRET_KEY environment variable")

    app.config.from_mapping(
        SECRET_KEY=secret_key,
        DATABASE=str(app_path / "data.db")
    )

    if test_config is not None:
        app.config.from_mapping(test_config)

    else:
        # Load config.py if it exists
        app.config.from_pyfile("config.py", silent=True)

    with app.app_context():
        init_app(app)

    app.register_blueprint(ui_bp)
    app.register_blueprint(api_bp)

    return app
